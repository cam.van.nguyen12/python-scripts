def urlify():
    input = "Mr. John Smith"
    result = input.replace(" ", "%20")
    print(result)
    
#urlify()


# O(sizeof(input))
def fill_dictionary(input):
    letters = dict()
    for i in input:
        if (i.isspace() == False) and not (i in letters.keys()):
            letters[i] = 1
        elif i in letters.keys():
            temp = letters[i]
            letters[i] =  temp + 1
    return letters

# O(sizeof(input))
def print_letters(letters):
    for i in letters.keys():
        print(i + " "+ str(letters[i]) )
    print("\n")

def all_even(input):
    return all( x % 2 == 0 for x in input)

def remove_value(dictionary, value):
    for key in dictionary.keys():
        if dictionary[key] == value:
            dictionary.pop(key)
            break

def parity_count(input):
    odd_count = 0
    even_count = 0
    for x in input:
        if x % 2 == 1:
            odd_count += 1
        else:
            even_count += 1
    # check if it has only one odd number of characters
    # and at least an even count of characters
    if odd_count == 1 and even_count >= 1:
        return True
    # check if it is all one kind of string
    elif all( x == list(input)[0] for x in input):
        return True
    else: 
        return False


# check if the string has all even number of characters
# or just one odd value
def  parity_check(input, dictionary):
    if all_even(dictionary.values()) and (len(input) % 2 == 0):
       print("True")
    elif parity_count(dictionary.values()):
       print("True")
    else:
        print("False")

# Check if the strings have the characterics of a palindrome
def palindrome_perm(input):
    #  Populate the message dictionary
    letters = fill_dictionary(input)
    # if it is even then you can have one on  each side
    parity_check(input, letters)

palindrome_perm("amanaplanacanalpanama")